use lambda_runtime::{handler_fn, Context, Error};
use serde_json::Value;
use tracing::{info, Level};

async fn function_handler(event: Value, _: Context) -> Result<Value, Error> {
    info!("Processing lambda request: {:?}", event);
    // Your logic here
    Ok(event)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt().with_max_level(Level::INFO).init();

    let func = handler_fn(function_handler);
    lambda_runtime::run(func).await?;
    Ok(())
}
