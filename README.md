# Rust Lambda Function
![pipeline status](https://gitlab.oit.duke.edu/jx139/ids721-mini-project-6/badges/main/pipeline.svg)

This repository contains a simple AWS Lambda function written in Rust using the `lambda_runtime` crate. The function logs incoming events and returns them back as responses.

## Setup

### Prerequisites

- Rust programming language installed (see [installation instructions](https://www.rust-lang.org/tools/install))
- AWS CLI installed and configured with appropriate credentials (see [installation instructions](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html))

### Building the Function

1. Clone this repository:

   ```bash
   git clone <repository_url>
   ```

2. Navigate to the project directory:

   ```bash
   cd rust-lambda-function
   ```

3. Build the Lambda function:

   ```bash
   cargo build --release --target x86_64-unknown-linux-musl
   ```

### Deploying to AWS Lambda

1. Package the Lambda function into a ZIP file:

   ```bash
   zip -j rust_lambda_function.zip ./target/x86_64-unknown-linux-musl/release/bootstrap
   ```

2. Create an IAM role with permissions for AWS Lambda execution and, for CloudWatch Logs and AWS X-Ray tracing.
    ![iam](./pic/iam.png)

3. Create the Lambda function using the AWS CLI:

   ```bash
   aws lambda create-function \
   --function-name rust_lambda_function \
   --handler doesnt.matter \
   --zip-file fileb://./rust_lambda_function.zip \
   --runtime provided.al2 \
   --role arn:aws:iam::<your_account_id>:role/<your_execution_role> \
   --environment Variables={RUST_BACKTRACE=1} \
   --tracing-config Mode=Active
   ```

   Replace `<your_account_id>` and `<your_execution_role>` with your AWS account ID and the ARN of the IAM role you created, respectively.
   ![lambda](./pic/lambda.png)
   ![cli](./pic/cli.png)

### Invoking the Lambda Function

You can invoke the Lambda function using the AWS CLI. For example:

```bash
aws lambda invoke --function-name rust_lambda_function --payload '{"key": "value"}' response.json
```

This command will invoke the Lambda function with the provided JSON payload and store the response in the `response.json` file.
![output](./pic/output.png)

### Monitoring and Troubleshooting

- Monitor function invocations, logs, and traces in the AWS Lambda console.
- Check CloudWatch Logs for function output and potential errors.
- Use AWS X-Ray for distributed tracing if active tracing is enabled.
![log](./pic/log.png)
![cloudwatch](./pic/cloudwatch.png)
